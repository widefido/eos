import eos

import time

LABEL_FOLLOW = 1
LABEL_INBOX  = 2
LABEL_OUTBOX = 3

LABEL_CREATE_LABEL = 4
LABEL_CREATE_TAG = 5

def now():
    return int(time.time() * 1000000)

class Activity:
    def __init__(self, client=None):
        if client is None:
            client = eos.Client()
        self.client = client

    def onew(self, otype, value=None):
        return self.client.onew(otype, value=value)

    def follow(self, consumer, producer, fanin=False, fanin_limit=100):
        result = self.client.gaddedge(consumer, LABEL_FOLLOW, producer, now())
        if fanin:
            self.client.gfanin(producer, LABEL_OUTBOX, LABEL_INBOX, consumer, limit=100)
        return result

    def unfollow(self, consumer, producer):
        return self.client.gremedge(consumer, LABEL_FOLLOW, producer)

    def followers(self, producer):
        return self.client.gedges(producer, LABEL_FOLLOW, incoming=True)

    def following(self, consumer):
        return self.client.gedges(consumer, LABEL_FOLLOW, incoming=False)

    def inbox(self, consumer):
        return self.client.gedges(consumer, LABEL_INBOX, incoming=True)

    def outbox(self, producer):
        return self.client.gedges(producer, LABEL_OUTBOX, incoming=False)

    def post(self, producer, value, fanout=True):
        weight = now()
        oid = self.client.onew("post", value)
        self.client.gaddedge(producer, LABEL_OUTBOX, oid, weight)
        if fanout:
            self.client.gfanout(producer, LABEL_FOLLOW, oid, LABEL_INBOX, weight, "", incoming=True)
        return oid

if __name__ == "__main__":
    a = Activity()
    user1 = 1000
    user2 = 1001
    feed1 = 2000
    feed2 = 2001
    feed3 = 2002
    label1 = 3000
    label2 = 3001

    a.follow(user1, feed1)
    a.follow(user1, feed2)
    a.follow(user1, feed3)
    a.follow(user2, feed2)

    a.follow(label1, feed1)
    a.follow(label2, feed2)
    
    #a.post(feed1, "test1")
    #a.post(feed2, "test2")
    #a.post(feed3, "test3")
    
    print("feed1", a.outbox(feed1))
    print("feed2", a.outbox(feed2))
    print("feed3", a.outbox(feed3))

    print("user1", a.inbox(user1))
    print("user2", a.inbox(user2))
