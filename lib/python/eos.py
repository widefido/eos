import redis

for k in list(redis.StrictRedis.RESPONSE_CALLBACKS.keys()):
    del redis.StrictRedis.RESPONSE_CALLBACKS[k]

class Client(redis.StrictRedis):
    """Redis Client for the eos server"""
    def __init__(self, *args, **kwargs):
        if "port" not in kwargs:
            kwargs["port"] = 9736
        super(Client, self).__init__(*args, **kwargs)

    def help(self, command=None):
        if command is not None:
            return self.execute_command("HELP", command)
        else:
            return self.execute_command("HELP")
        
    def ping(self):
        return self.execute_command("PING")

    def flake(self):
        return self.execute_command("FLAKE")

    def onew(self, otype, value=None):
        if value is None:
            return self.execute_command("ONEW", otype)
        return self.execute_command("ONEW", otype, value)
    
    def oget(self, oid):
        return self.execute_command("OGET", oid)

    def oset(self, oid, value):
        return self.execute_command("OSET", oid, value)

    def orem(self, oid):
        return self.execute_command("OREM", oid)

    def oaddindex(self, oid, key, value):
        return self.execute_command("OADDINDEX", oid, key, value)

    def oremindex(self, oid, key):
        return self.execute_command("OREMINDEX", oid, key)

    def ogetbyindex(self, key, minValue=None, maxValue=None, limit=0):
        args = []
        if minValue is not None and maxValue is not None:
            args.extend(("BETWEEN", minValue, maxValue))
        if limit:
            args.extend(("LIMIT", limit))
        return self.execute_command("OGETBYINDEX", key, *args)

    def gaddedge(self, vertexFrom, label, vertexTo, weight=0, value=None):
        if value is None:
            return self.execute_command("GADDEDGE", vertexFrom, label, vertexTo, weight)
        return self.execute_command("GADDEDGE", vertexFrom, label, vertexTo, weight, value)

    def gremedge(self, vertexFrom, label, vertexTo):
        return self.execute_command("GREMEDGE", vertexFrom, label, vertexTo)

    def gedge(self, vertexFrom, label, vertexTo):
        return self.execute_command("GEDGE", vertexFrom, label, vertexTo)

    def gedges(self, vertexFrom, label, incoming=False, minWeight=None, maxWeight=None, limit=0):
        args = []
        direction = "INCOMING" if incoming else "OUTGOING" 
        if minWeight is not None and maxWeight is not None:
            args.extend(("BETWEEN", minWeight, maxWeight))
        if limit:
            args.extend(("LIMIT", limit))
        return self.execute_command("GEDGES", vertexFrom, direction, label, *args)

    def gcountedges(self, vertexFrom, label, incoming=False, minWeight=None, maxWeight=None, limit=0):
        args = []
        direction = "INCOMING" if incoming else "OUTGOING" 
        if minWeight is not None and maxWeight is not None:
            args.extend(("BETWEEN", minWeight, maxWeight))
        if limit:
            args.extend(("LIMIT", limit))
        return self.execute_command("GCOUNTEDGES", vertexFrom, direction, label, *args)

    def gremedges(self, vertexFrom, label, incoming=False, minWeight=None, maxWeight=None, limit=0):
        args = []
        direction = "INCOMING" if incoming else "OUTGOING" 
        if minWeight is not None and maxWeight is not None:
            args.extend(("BETWEEN", minWeight, maxWeight))
        if limit:
            args.extend(("LIMIT", limit))
        return self.execute_command("GREMEDGES", vertexFrom, direction, label, *args)

    def gcard(self, vertexFrom, label, incoming=False):
        direction = "INCOMING" if incoming else "OUTGOING" 
        return self.execute_command("GCARD", vertexFrom, direction, label)

    def gfanout(self, vertexFrom, label, newVertexFrom, newLabel, newWeight, newValue, remove=False, incoming=False, minWeight=None, maxWeight=None, limit=0):
        args = []
        direction = "INCOMING" if incoming else "OUTGOING" 
        if minWeight is not None and maxWeight is not None:
            args.extend(("BETWEEN", minWeight, maxWeight))
        if limit:
            args.extend(("LIMIT", limit))
        action = "REM" if remove else "ADD"
        args.extend((action, newVertexFrom, newLabel, newWeight, newValue))
        return self.execute_command("GFANOUT", vertexFrom, direction, label, *args) 

    def gfanin(self, vertexFrom, label, newLabel, newVertexTo, remove=False, incoming=False, minWeight=None, maxWeight=None, limit=0):
        args = []
        direction = "INCOMING" if incoming else "OUTGOING" 
        if minWeight is not None and maxWeight is not None:
            args.extend(("BETWEEN", minWeight, maxWeight))
        if limit:
            args.extend(("LIMIT", limit))
        action = "REM" if remove else "ADD"
        args.extend((action, newLabel, newVertexTo))
        return self.execute_command("GFANIN", vertexFrom, direction, label, *args) 

    def pipeline(self, transaction=False, shard_hint=None):
        if transaction:
            raise Exception("EOS pipelines do not support transactions")
        return Pipeline(
            self.connection_pool,
            self.response_callbacks,
            transaction,
            shard_hint)

class Pipeline(redis.client.BasePipeline, Client):
    """Redis Pipeline for the eos server"""
    pass

