package server

import (
	"bitbucket.org/widefido/2i/server"
	"bitbucket.org/widefido/eos/storage"

	"github.com/rcrowley/go-metrics"

	"bytes"
	"fmt"
	"runtime"
	"sort"
	"time"
)

////////////
// Limits //
////////////

const MaxInt64 = int64(^uint64(0) >> 1)
const MinInt64 = -(MaxInt64 - 1)

var MaxByteSlice = []byte{0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
var MinByteSlice = []byte{0x00}

///////////////
// EOSServer //
///////////////

type EOSServer struct {
	s     *server.Server
	store storage.Storage
}

func New(config server.ServerConfig, store storage.Storage) (*EOSServer, error) {
	eos := &EOSServer{
		s:     server.NewServer(config),
		store: store,
	}

	handlers := []server.CommandHandler{
		server.CommandHandlerFunc("INFO", "INFO", 0, false, eos.HandleInfo),

		server.CommandHandlerFunc("FLAKE", "FLAKE", 0, false, eos.HandleFlake),
		server.CommandHandlerFunc("ONEW", "ONEW type [value]", -1, true, eos.HandleNewObject),
		server.CommandHandlerFunc("OGET", "OGET id", 1, false, eos.HandleGetObject),
		server.CommandHandlerFunc("OSET", "OSET id value", 2, true, eos.HandleSetObject),
		server.CommandHandlerFunc("OREM", "OREM id", 1, true, eos.HandleRemObject),
		server.CommandHandlerFunc("OADDINDEX", "OADDINDEX id key value", 3, true, eos.HandleAddObjectIndex),
		server.CommandHandlerFunc("OREMINDEX", "OREMINDEX id key", 2, true, eos.HandleRemObjectIndex),
		server.CommandHandlerFunc("OGETBYINDEX", "OGETBYINDEX key value [LIMIT limit]", -2, true, eos.HandleGetObjectsByIndex),

		server.CommandHandlerFunc("GADDEDGE", "GADDEDGE from label to [weight] [value]", -3, true, eos.HandleAddEdge),
		server.CommandHandlerFunc("GREMEDGE", "GREMEDGE from label to", 3, true, eos.HandleRemEdge),
		server.CommandHandlerFunc("GEDGE", "GEDGE from (OUTGOING|INCOMING) label to", 4, false, eos.HandleGetEdge),
		server.CommandHandlerFunc("GEDGES", "GEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]", -3, false, eos.HandleGetEdges),
		server.CommandHandlerFunc("GEDGESBYVERTEX", "GEDGESBYVERTEX from (OUTGOING|INCOMING) label [BETWEEN minTo maxTo] [LIMIT limit]", -3, false, eos.HandleGetEdgesByVertex),
		server.CommandHandlerFunc("GCOUNTEDGES", "GCOUNTEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]", -3, false, eos.HandleCountEdges),
		server.CommandHandlerFunc("GREMEDGES", "GREMEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]", -3, true, eos.HandleRemEdges),
		server.CommandHandlerFunc("GCARD", "GCARD from (OUTGOING|INCOMING) label", 3, false, eos.HandleGetCard),
		server.CommandHandlerFunc("GFANOUT", "GFANOUT from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit] (ADD|REM) newFrom newLabel [newWeight] [newValue]", -6, true, eos.HandleFanout),
		server.CommandHandlerFunc("GFANIN", "GFANIN from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit] (ADD|REM) newLabel newTo", -6, true, eos.HandleFanin),
	}

	eos.s.RegisterHandlers(handlers...)

	return eos, nil
}

func (self *EOSServer) Serve() error {
	return self.s.Serve()
}

func (self *EOSServer) Stop() {
	self.s.Stop()
}

func (self *EOSServer) Close() error {
	return self.s.Close()
}

//////////////////////
// Command Handlers //
//////////////////////

func (self *EOSServer) HandleInfo(client *server.Client, request *server.Request) error {
	newline := []byte("\n")

	info := bytes.Join([][]byte{
		[]byte("# EOS"),
		[]byte(fmt.Sprintf("storage_type: %s", self.store.Type())),
		[]byte(fmt.Sprintf("storage_path: %s", self.store.Path())),
		[]byte(""),
	}, newline)

	var stats runtime.MemStats
	runtime.ReadMemStats(&stats)

	info = bytes.Join([][]byte{
		info,
		[]byte("# EOS Memory"),
		[]byte(fmt.Sprintf("mem_sys: %d", stats.Sys)),
		[]byte(fmt.Sprintf("mem_sys_human: %s", server.ByteSize(stats.Sys))),
		[]byte(fmt.Sprintf("mem_inuse: %d", stats.Alloc)),
		[]byte(fmt.Sprintf("mem_inuse_human: %s", server.ByteSize(stats.Alloc))),
		[]byte(fmt.Sprintf("mem_inuse_stack: %d", stats.StackInuse)),
		[]byte(fmt.Sprintf("mem_inuse_stack_human: %s", server.ByteSize(stats.StackInuse))),
		[]byte(fmt.Sprintf("mem_inuse_heap: %d", stats.HeapAlloc)),
		[]byte(fmt.Sprintf("mem_inuse_heap_human: %s", server.ByteSize(stats.HeapAlloc))),
		[]byte(fmt.Sprintf("mem_mallocs: %d", stats.Mallocs)),
		[]byte(fmt.Sprintf("mem_frees: %d", stats.Frees)),
		[]byte(fmt.Sprintf("mem_gc_count: %d", stats.NumGC)),
		[]byte(fmt.Sprintf("mem_gc_pause_last: %s", time.Duration(stats.PauseNs[(stats.NumGC+255)%256]))),
		[]byte(fmt.Sprintf("mem_gc_pause_total: %s", time.Duration(stats.PauseTotalNs))),
		[]byte(""),
	}, newline)

	output := make([][]byte, 0)
	self.s.Metrics().Each(func(name string, i interface{}) {
		output = append(output, []byte(self.writeMetric(name, i)))
	})

	sort.Sort(server.ByteSliceSlice(output))

	if len(output) > 0 {
		info = bytes.Join([][]byte{
			info,
			[]byte("# EOS Metrics"),
			bytes.Join(output, newline),
			[]byte(""),
		}, newline)
	}

	client.SendReply(info)
	return nil

}

func (self *EOSServer) writeMetric(name string, i interface{}) string {
	switch metric := i.(type) {
	case metrics.Meter:
		m := metric.Snapshot()
		return fmt.Sprintf("%s: count=%d 1min-r/s=%.2f 5min-r/s=%.2f 15min-r/s=%.2f mean-r/s=%.2f", name, m.Count(), m.Rate1(), m.Rate5(), m.Rate15(), m.RateMean())
	case metrics.Timer:
		t := metric.Snapshot()
		ps := t.Percentiles([]float64{0.5, 0.75, 0.95, 0.99, 0.999})
		return fmt.Sprintf("%s: count=%d 95th=%s 99th=%s 99.9th=%s mean-r/s=%.2f", name, t.Count(), time.Duration(ps[2]).String(), time.Duration(ps[3]).String(), time.Duration(ps[4]).String(), t.RateMean())
	}
	return ""
}

// FLAKE
// return a new unique id
// @returns <int> unique 63-bit integer
func (self *EOSServer) HandleFlake(client *server.Client, request *server.Request) error {
	id := self.store.GetUniqueId()
	client.SendReply(id)
	return nil
}

// ONEW type [value]
// create a new object with the provided value, assigining it a unique id
// @returns <integer> unique id of the new object
func (self *EOSServer) HandleNewObject(client *server.Client, request *server.Request) error {
	otype, oErr := request.ArgString(0)
	if oErr != nil {
		return oErr
	}
	value, _ := request.ArgBytes(1)

	obj, err := self.store.NewObject(otype, value)
	if err != nil {
		return err
	}

	client.SendReply(obj.Id)
	return nil
}

// OGET id
// get an objects type and value
// @returns <multi> (type, value) of the object
func (self *EOSServer) HandleGetObject(client *server.Client, request *server.Request) error {
	id, _ := request.ArgInt64(0)

	obj, err := self.store.GetObject(id)
	if err != nil {
		return err
	}

	if obj == nil {
		client.SendReply(nil)
		return nil
	}

	client.SendReply([]interface{}{obj.Type, obj.Value})
	return nil
}

// OSET id value
// update an objects value
// @returns <string> OK
func (self *EOSServer) HandleSetObject(client *server.Client, request *server.Request) error {
	id, _ := request.ArgInt64(0)
	value, _ := request.ArgBytes(1)

	err := self.store.SetObject(id, value)
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}

// OREM id
// remove an object from the graph (and its edges?)
// @returns <string> OK
func (self *EOSServer) HandleRemObject(client *server.Client, request *server.Request) error {
	id, _ := request.ArgInt64(0)

	err := self.store.RemObject(id)
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}

// OADDINDEX id key value
// add an index entry for an object (a key->value pair for quick indexed lookups)
// @returns <string> OK
func (self *EOSServer) HandleAddObjectIndex(client *server.Client, request *server.Request) error {
	id, idErr := request.ArgInt64(0)
	key, keyErr := request.ArgString(1)
	value, _ := request.ArgBytes(2)

	if idErr != nil || keyErr != nil {
		return server.ErrRequestWrongArgs
	}

	err := self.store.AddObjectIndex(id, key, value)
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}

// OREMINDEX id key
// remove index entry for an object
// @returns <string> OK
func (self *EOSServer) HandleRemObjectIndex(client *server.Client, request *server.Request) error {
	id, idErr := request.ArgInt64(0)
	key, keyErr := request.ArgString(1)

	if idErr != nil || keyErr != nil {
		return server.ErrRequestWrongArgs
	}

	err := self.store.RemObjectIndex(id, key)
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}

// OGETBYINDEX key value [LIMIT limit]
// get a list of object ids that are indexed with (key, value) pairs
// @returns <multi> list of object ids
func (self *EOSServer) HandleGetObjectsByIndex(client *server.Client, request *server.Request) error {
	key, keyErr := request.ArgString(0)

	value, valErr := request.ArgBytes(1)
	if valErr != nil {
		return valErr
	}

	limit := int64(1000)
	if request.ArgMatch(1, "LIMIT", true) {
		limit, _ = request.ArgInt64(2)
	} else if request.ArgMatch(4, "LIMIT", true) {
		limit, _ = request.ArgInt64(5)
	}

	if keyErr != nil {
		return server.ErrRequestWrongArgs
	}

	objects, err := self.store.GetObjectsByIndex(key, value, limit)
	if err != nil {
		return err
	}

	if objects == nil {
		client.SendReply(nil)
		return nil
	}

	values := make([]interface{}, 0)
	for _, object := range objects {
		values = append(values, object.Id)
	}

	client.SendReply(values)
	return nil
}

// GADDEDGE from label to [weight] [value]
// add an edge between to vertices
// @returns <string> OK
func (self *EOSServer) HandleAddEdge(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	label, labelErr := request.ArgInt64(1)
	to, toErr := request.ArgInt64(2)
	weight, _ := request.ArgInt64(3)
	value, _ := request.ArgBytes(4)

	if fromErr != nil || labelErr != nil || toErr != nil {
		return server.ErrRequestWrongArgs
	}

	err := self.store.AddEdge(from, label, weight, to, value)
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}

// GREMEDGE from label to
// remove an edge between to vertices
// @returns <string> OK
func (self *EOSServer) HandleRemEdge(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	label, labelErr := request.ArgInt64(1)
	to, toErr := request.ArgInt64(2)

	if fromErr != nil || labelErr != nil || toErr != nil {
		return server.ErrRequestWrongArgs
	}

	err := self.store.RemEdge(from, label, to)
	if err != nil {
		return err
	}

	client.SendReply("OK")
	return nil
}

// GEDGE from (OUTGOING|INCOMING) label to
// get the weight and value of an edge
// @returns <multi> tuple of (weight, value) of the edge
func (self *EOSServer) HandleGetEdge(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	to, toErr := request.ArgInt64(3)

	if fromErr != nil || labelErr != nil || toErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	edge, err := self.store.GetEdge(storage.EdgeState(state), from, label, to)
	if err != nil {
		return err
	}

	if edge == nil {
		client.SendReply(nil)
		return nil
	}

	client.SendReply([]interface{}{edge.Weight, edge.Value})
	return nil
}

// GEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]
// get the edges extending from a vertex in a given direction with a specific label
// @returns <multi<multi>> list of tuples of (to, weight, value)
func (self *EOSServer) HandleGetEdges(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	if fromErr != nil || labelErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	minWeight := MinInt64
	maxWeight := MaxInt64
	if request.ArgMatch(3, "BETWEEN", true) {
		var err error
		minWeight, err = request.ArgInt64(4)
		if err != nil {
			minWeight = MinInt64
		}
		maxWeight, err = request.ArgInt64(5)
		if err != nil {
			maxWeight = MaxInt64
		}
	}

	limit := int64(1000)
	if request.ArgMatch(3, "LIMIT", true) {
		limit, _ = request.ArgInt64(4)
	} else if request.ArgMatch(6, "LIMIT", true) {
		limit, _ = request.ArgInt64(7)
	}

	edges, err := self.store.GetEdges(storage.EdgeState(state), from, label, minWeight, maxWeight, limit)
	if err != nil {
		return err
	}

	if edges == nil {
		client.SendReply(nil)
		return nil
	}

	values := make([]interface{}, 0)

	for _, edge := range edges {
		values = append(values, []interface{}{edge.To, edge.Weight, edge.Value})
	}

	client.SendReply(values)
	return nil
}

// GEDGESBYVERTEX from (OUTGOING|INCOMING) label [BETWEEN minTo maxTo] [LIMIT limit]
// get the edges extending from a vertex in a given direction with a specific label
// @returns <multi<multi>> list of tuples of (to, weight, value)
func (self *EOSServer) HandleGetEdgesByVertex(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	if fromErr != nil || labelErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	minTo := MinInt64
	maxTo := MaxInt64
	if request.ArgMatch(3, "BETWEEN", true) {
		var err error
		minTo, err = request.ArgInt64(4)
		if err != nil {
			minTo = MinInt64
		}
		maxTo, err = request.ArgInt64(5)
		if err != nil {
			maxTo = MaxInt64
		}
	}

	limit := int64(1000)
	if request.ArgMatch(3, "LIMIT", true) {
		limit, _ = request.ArgInt64(4)
	} else if request.ArgMatch(6, "LIMIT", true) {
		limit, _ = request.ArgInt64(7)
	}

	edges, err := self.store.GetEdgesByVertex(storage.EdgeState(state), from, label, minTo, maxTo, limit)
	if err != nil {
		return err
	}

	if edges == nil {
		client.SendReply(nil)
		return nil
	}

	values := make([]interface{}, 0)

	for _, edge := range edges {
		values = append(values, []interface{}{edge.To, edge.Weight, edge.Value})
	}

	client.SendReply(values)
	return nil
}

// GCOUNTEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]
// count the edges extending from a vertex in a given direction with a specific label
// @returns <integer> number of edges in the range
func (self *EOSServer) HandleCountEdges(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	if fromErr != nil || labelErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	minWeight := MinInt64
	maxWeight := MaxInt64
	if request.ArgMatch(3, "BETWEEN", true) {
		var err error
		minWeight, err = request.ArgInt64(4)
		if err != nil {
			minWeight = MinInt64
		}
		maxWeight, err = request.ArgInt64(5)
		if err != nil {
			maxWeight = MaxInt64
		}
	}

	limit := int64(1000)
	if request.ArgMatch(3, "LIMIT", true) {
		limit, _ = request.ArgInt64(4)
	} else if request.ArgMatch(6, "LIMIT", true) {
		limit, _ = request.ArgInt64(7)
	}

	count, err := self.store.CountEdges(storage.EdgeState(state), from, label, minWeight, maxWeight, limit)
	if err != nil {
		return err
	}

	client.SendReply(count)
	return nil
}

// GREMEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]
// remove the edges extending from a vertex in a given direction with a specific label
// @returns <integer> number of edges removed in the range
func (self *EOSServer) HandleRemEdges(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	if fromErr != nil || labelErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	minWeight := MinInt64
	maxWeight := MaxInt64
	if request.ArgMatch(3, "BETWEEN", true) {
		var err error
		minWeight, err = request.ArgInt64(4)
		if err != nil {
			minWeight = MinInt64
		}
		maxWeight, err = request.ArgInt64(5)
		if err != nil {
			maxWeight = MaxInt64
		}
	}

	limit := int64(1000)
	if request.ArgMatch(3, "LIMIT", true) {
		limit, _ = request.ArgInt64(4)
	} else if request.ArgMatch(6, "LIMIT", true) {
		limit, _ = request.ArgInt64(7)
	}

	count, err := self.store.RemoveEdges(storage.EdgeState(state), from, label, minWeight, maxWeight, limit)
	if err != nil {
		return err
	}

	client.SendReply(count)
	return nil
}

// GCARD from (OUTGOING|INCOMING) label
// get the estimated cardinality of nodes outgoing or incoming to a vertex with the specified label
// @returns <integer> cardinality
func (self *EOSServer) HandleGetCard(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	if fromErr != nil || labelErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	card, err := self.store.Cardinality(storage.EdgeState(state), from, label)
	if err != nil {
		return err
	}

	client.SendReply(card)
	return nil
}

// GFANOUT from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit] (ADD|REM) newFrom newLabel [newWeight] [newValue]
// select a range of edges and create a new list of edges from that range, using the vertices returned as the "to" node
// @returns <integer> the number of edges created
func (self *EOSServer) HandleFanout(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	if fromErr != nil || labelErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	asPos := 3 // or 6 or 8

	minWeight := MinInt64
	maxWeight := MaxInt64
	if request.ArgMatch(3, "BETWEEN", true) {
		var err error
		minWeight, err = request.ArgInt64(4)
		if err != nil {
			minWeight = MinInt64
		}
		maxWeight, err = request.ArgInt64(5)
		if err != nil {
			maxWeight = MaxInt64
		}
		asPos += 3
	}

	limit := int64(1000)
	if request.ArgMatch(3, "LIMIT", true) {
		limit, _ = request.ArgInt64(4)
		asPos += 2
	} else if request.ArgMatch(6, "LIMIT", true) {
		limit, _ = request.ArgInt64(7)
		asPos += 2
	}

	action := request.ArgMatchFirst(asPos, []string{"ADD", "REM"}, true)
	if action == -1 {
		return server.ErrRequestWrongArgs
	}

	newFrom, newFromErr := request.ArgInt64(asPos + 1)
	newLabel, newLabelErr := request.ArgInt64(asPos + 2)
	newWeight, _ := request.ArgInt64(asPos + 3)
	newValue, _ := request.ArgBytes(asPos + 4)
	if newFromErr != nil || newLabelErr != nil {
		return server.ErrRequestWrongArgs
	}

	count, err := self.store.Fanout(storage.EdgeAction(action), storage.EdgeState(state), from, label, minWeight, maxWeight, limit, newFrom, newLabel, newWeight, newValue)
	if err != nil {
		return err
	}

	client.SendReply(count)
	return nil
}

// GFANIN from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit] (ADD|REM) newLabel newTo
// select a range of edges and create a new list of edges from that range, using the vertices returned as the "from" node, to the newTo vertex
// @returns <integer> the number of edges created
func (self *EOSServer) HandleFanin(client *server.Client, request *server.Request) error {
	from, fromErr := request.ArgInt64(0)
	state := request.ArgMatchFirst(1, []string{"OUTGOING", "INCOMING"}, true)
	label, labelErr := request.ArgInt64(2)
	if fromErr != nil || labelErr != nil || state == -1 {
		return server.ErrRequestWrongArgs
	}

	asPos := 3 // or 6 or 8

	minWeight := MinInt64
	maxWeight := MaxInt64
	if request.ArgMatch(3, "BETWEEN", true) {
		var err error
		minWeight, err = request.ArgInt64(4)
		if err != nil {
			minWeight = MinInt64
		}
		maxWeight, err = request.ArgInt64(5)
		if err != nil {
			maxWeight = MaxInt64
		}
		asPos += 3
	}

	limit := int64(1000)
	if request.ArgMatch(3, "LIMIT", true) {
		limit, _ = request.ArgInt64(4)
		asPos += 2
	} else if request.ArgMatch(6, "LIMIT", true) {
		limit, _ = request.ArgInt64(7)
		asPos += 2
	}

	action := request.ArgMatchFirst(asPos, []string{"ADD", "REM"}, true)
	if action == -1 {
		return server.ErrRequestWrongArgs
	}

	newLabel, newLabelErr := request.ArgInt64(asPos + 1)
	newTo, newToErr := request.ArgInt64(asPos + 2)
	if newLabelErr != nil || newToErr != nil {
		return server.ErrRequestWrongArgs
	}

	count, err := self.store.Fanin(storage.EdgeAction(action), storage.EdgeState(state), from, label, minWeight, maxWeight, limit, newLabel, newTo)
	if err != nil {
		return err
	}

	client.SendReply(count)
	return nil
}

/////////////
// Exports //
/////////////

func OnSystemQuitSignal(callback func()) {
	server.OnSystemQuitSignal(callback)
}

func DefaultServerConfig() server.ServerConfig {
	return server.DefaultServerConfig()
}
