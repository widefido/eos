# eos - extended object storage

a simple prototype graph object store atop Cassandra modeled after Facebook's TAO

## schema

    see SCHEMA.cql

## api

- FLAKE
    return a new unique id
    @returns <integer> a new unique id (63-bit integer)

- ONEW type [value]
    create a new object with the provided value, assigining it a unique id
    @returns <integer> unique id of the new object

- OGET id
    get an objects type and value
    @returns <multi> (type, value) of the object

- OSET id value
    update an objects value
    @returns <string> OK

- OREM id 
    remove an object from the graph (and its edges?)
    @returns <string> OK

- OADDINDEX id key value
    add an index entry for an object
    @returns <string> OK

- OREMINDEX id key
    remove an index entry for an object
    @returns <string> OK

- OGETBYINDEX key value [LIMIT limit]
    get a list of objects that are indexed with values in the range
    @returns <multi> list of object ids

- GADDEDGE from label to [weight] [value]
    add an edge between to vertices
    @returns <string> OK

- GREMEDGE from label to
    remove an edge between to vertices
    @returns <string> OK

- GEDGE from (OUTGOING|INCOMING) label to
    get the weight and value of an edge
    @returns <multi> tuple of (weight, value) of the edge

- GEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]
    get the edges extending from a vertex in a given direction with a specific label
    @returns <multi<multi>> list of tuples of (to, weight, value)

- GEDGESBYVERTEX from (OUTGOING|INCOMING) label [BETWEEN minTo maxTo] [LIMIT limit]
    get the edges extending from a vertex in a given direction with a specific label
    @returns <multi<multi>> list of tuples of (to, weight, value)

- GCOUNTEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]
    count the edges extending from a vertex in a given direction with a specific label
    @returns <integer> number of edges in the range

- GREMEDGES from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit]
    remove the edges extending from a vertex in a given direction with a specific label
    @returns <integer> number of edges removed in the range

- GCARD from (OUTGOING|INCOMING) label
    get the estimated cardinality of nodes outgoing or incoming to a vertex with the specified label
    @returns <integer> cardinality

- GFANOUT from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit] (ADD|REM) newFrom newLabel [newWeight] [newValue]
    select a range of edges and create a new list of edges from that range, using the vertices returned as the "to" node
    @returns <integer> the number of edges created

- GFANIN from (OUTGOING|INCOMING) label [BETWEEN minWeight maxWeight] [LIMIT limit] (ADD|REM) newLabel newTo
    select a range of edges and create a new list of edges from that range, using the vertices returned as the "from" node
    @returns <integer> the number of edges created