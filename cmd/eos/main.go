package main

import (
	"bitbucket.org/widefido/2i/log"

	"bitbucket.org/widefido/eos/server"
	"bitbucket.org/widefido/eos/storage"

	"github.com/jessevdk/go-flags"

	"fmt"
	"net"
	"os"
	"runtime"
)

const __version__ = "0.0.1"
const __author__ = "Jordan Sherer <jordan@widefido.com>"

/////////
// cli //
/////////

var options struct {
	Verbose      []bool   `short:"v" long:"verbose" description:"Print verbose log messages"`
	Quiet        bool     `short:"q" long:"quiet" description:"Silence all log messages"`
	Metrics      bool     `short:"m" long:"metrics" description:"Print metrics"`
	Config       string   `short:"c" long:"config" description:"Path to configuration ini file" no-ini:"yes"`
	WriteConfig  bool     `long:"write-config" description:"Write updated settings to the configuration file" no-ini:"yes"`
	ListenHost   string   `short:"h" long:"host" description:"Listen host" default:"127.0.0.1"`
	ListenPort   string   `short:"p" long:"port" description:"Listen port" default:"9736"`
	ListenSocket string   `short:"s" long:"socket" description:"Listen socket path (overrides host and port)"`
	Keyspace     string   `long:"keyspace" description:"Cassandra keyspace" default:"eos"`
	Nodes        []string `long:"node" description:"Cassandra node addresses"`
	MaxProcs     int      `long:"max-procs" description:"Number of true threads to use for parallelism (specify 0 for NumCPU)" default:"1" no-ini:"yes"`
	Version      bool     `long:"version" description:"Output the version of the 2i library" no-ini:"yes"`
	Help         bool     `long:"help" description:"Show this help message" no-ini:"yes"`
}

func main() {
	p := flags.NewParser(&options, flags.PrintErrors|flags.PassDoubleDash)
	p.Usage = "[OPTIONS]"

	// parse command options
	_, err := p.Parse()
	if err != nil || options.Help {
		if err != nil {
			fmt.Println("")
		}
		p.WriteHelp(os.Stdout)
		os.Exit(1)
		return
	}
	iniParser := flags.NewIniParser(p)

	// output version
	if options.Version {
		fmt.Printf("%s\n", __version__)
		os.Exit(0)
		return
	}

	// set the maximum number of true threads
	if options.MaxProcs == 0 {
		runtime.GOMAXPROCS(runtime.NumCPU())
	} else {
		runtime.GOMAXPROCS(1)
	}

	// set the log levels
	verbose := len(options.Verbose)
	if options.Quiet {
		log.SetLevel(log.CRITICAL)
	} else if verbose == 0 {
		log.SetLevel(log.INFO)
	} else if verbose == 1 {
		log.SetLevel(log.DEBUG)
	} else {
		log.SetLevel(log.NOTSET)
	}

	// lookup options from config file
	if len(options.Config) != 0 {
		config := options.Config
		err := iniParser.ParseFile(config)
		if err != nil {
			log.Error(err.Error())
		}
		if options.WriteConfig {
			iniParser.WriteFile(config, flags.IniIncludeDefaults|flags.IniIncludeComments)
		}
	}

	// figure out where we are going to listen
	protocol := "tcp"
	address := net.JoinHostPort(options.ListenHost, options.ListenPort)
	if len(options.ListenSocket) != 0 {
		protocol = "unix"
		address = options.ListenSocket
	}

	// configure the redis server and serve
	config := server.DefaultServerConfig()
	config.Name = "eos"
	config.Protocol = protocol
	config.ListenAddr = address
	config.WriteMetrics = options.Metrics

    cassandraConfig := storage.DefaultCassandraStorageConfig()
    cassandraConfig.Keyspace = options.Keyspace
    cassandraConfig.Nodes = options.Nodes

	// open the cassandra store
	store, err := storage.OpenCassandra(cassandraConfig)
	if err != nil {
		log.Fatal(err)
		return
	}

	s, err := server.New(config, store)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer s.Close()

	go server.OnSystemQuitSignal(s.Stop)

	s.Serve()
}
