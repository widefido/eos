TEST=.
BENCH=.
COVERPROFILE=/tmp/c.out
BRANCH=`git rev-parse --abbrev-ref HEAD`
COMMIT=`git rev-parse --short HEAD`
CWD=`pwd`
GOLDFLAGS="-X main.branch $(BRANCH) -X main.commit $(COMMIT)"
GOPATH=$(CWD)/.gopath

default: build

clean:
	@rm -f bin/*

# http://cloc.sourceforge.net/
cloc:
	@cloc --exclude-dir=env32,env33,env34,.gopath --not-match-f='Makefile|_test.go' .

fmt:
	@GOPATH=$(GOPATH) go fmt ./...

get:
	@GOPATH=$(GOPATH) go get -v golang.org/x/tools/cmd/cover
	@GOPATH=$(GOPATH) go get -v golang.org/x/tools/cmd/vet
	@GOPATH=$(GOPATH) go get -v github.com/davecheney/gcvis
	@GOPATH=$(GOPATH) go get -a -d -v ./...

get-update:
	@GOPATH=$(GOPATH) go get -a -u -v golang.org/x/tools/cmd/cover
	@GOPATH=$(GOPATH) go get -a -u -v golang.org/x/tools/cmd/vet
	@GOPATH=$(GOPATH) go get -a -u -v github.com/davecheney/gcvis
	@GOPATH=$(GOPATH) go get -a -u -d -v ./...

gopath:
	@mkdir -p $(GOPATH)/src/bitbucket.org/widefido
	@ln -s ../../../../ $(GOPATH)/src/bitbucket.org/widefido/eos

bench: fmt
	GOPATH=$(GOPATH) go test -v -test.run=NOTHINCONTAINSTHIS -test.bench=$(BENCH) -test.benchmem -test.benchtime "2s" ./...

test: fmt
	@echo "=== TESTS ==="
	@GOPATH=$(GOPATH) go test -v -cover -test.run=$(TEST) ./...

cover: fmt
	GOPATH=$(GOPATH) go test -coverprofile=$(COVERPROFILE) -test.run=$(TEST) $(COVERFLAG) .
	GOPATH=$(GOPATH) go tool cover -html=$(COVERPROFILE)
	rm $(COVERPROFILE)

vet:
	GOPATH=$(GOPATH) go vet ./...

todo:
	@git grep --untracked TODO

build: server

server: get
	@mkdir -p bin
	@GOPATH=$(GOPATH) go build -ldflags=$(GOLDFLAGS) -a -o bin/eos ./cmd/eos/main.go

all: build

.PHONY: clean bench cloc cover fmt get build test run
