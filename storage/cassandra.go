package storage

import (
	"github.com/gocql/gocql"

	"bitbucket.org/widefido/2i/keys"
	//"bitbucket.org/widefido/2i/log"

	"errors"
	"fmt"
	"strings"
)

var (
	ErrNotImplemented    = errors.New("Not implemented.")
	ErrCannotUnserialize = errors.New("Cannot unserialize.")
)

////////////
// Config //
////////////

type CassandraStorageConfig struct {
	Keyspace          string
	Nodes             []string
	WriteConsistency  gocql.Consistency
	ReadConsistency   gocql.Consistency
	StoreCardinality  bool
	PageSize          int
	PagePrefetchRatio float64
}

func DefaultCassandraStorageConfig() CassandraStorageConfig {
	return CassandraStorageConfig{
		WriteConsistency:  gocql.One,
		ReadConsistency:   gocql.One,
		StoreCardinality:  true,
		PageSize:          1000,
		PagePrefetchRatio: 0.5,
	}
}

///////////////////////
// Cassandra Storage //
///////////////////////

type CassandraStorage struct {
	cluster *gocql.ClusterConfig
	session *gocql.Session
	config  CassandraStorageConfig
}

func OpenCassandra(config CassandraStorageConfig) (*CassandraStorage, error) {
	cluster := gocql.NewCluster(config.Nodes...)
	cluster.Keyspace = config.Keyspace

	session, err := cluster.CreateSession()
	if err != nil {
		return nil, err
	}

	return &CassandraStorage{
		cluster: cluster,
		session: session,
	}, nil
}

func (self *CassandraStorage) Close() error {
	if self.session != nil {
		self.session.Close()
		self.session = nil
	}

	return nil
}

/////////////////////
// Storage Methods //
/////////////////////

func (self *CassandraStorage) Type() string {
	return "cassandra"
}

func (self *CassandraStorage) Path() string {
	return fmt.Sprintf("cassandra://%s?keyspace=%s", strings.Join(self.cluster.Hosts, ","), self.cluster.Keyspace)
}

func (self *CassandraStorage) GetUniqueId() int64 {
	// since flakes (by default) use a boolean bit, we can reduce the id to a 63 bit int
	return int64((keys.Flake.Generate() >> 1) & 0xFFFFFFFFFFFFFFFF)
}

func (self *CassandraStorage) NewObject(otype string, value []byte) (*Object, error) {
	id := self.GetUniqueId()

	q := self.WriteQuery("INSERT INTO object_values (id, type, value) VALUES (?, ?, ?) IF NOT EXISTS", id, otype, value)

	err := q.Exec()
	if err != nil {
		return nil, err
	}

	return &Object{
		Id:    id,
		Type:  otype,
		Value: value,
	}, nil
}

func (self *CassandraStorage) GetObject(id int64) (*Object, error) {
	q := self.ReadQuery("SELECT type, value FROM object_values WHERE id = ?", id)
	q = q.PageSize(0)

	i := q.Iter()

	var otype string
	var value []byte
	if !i.Scan(&otype, &value) {
		return nil, i.Close()
	}

	return &Object{
		Id:    id,
		Type:  otype,
		Value: value,
	}, nil
}

func (self *CassandraStorage) SetObject(id int64, value []byte) error {
	q := self.WriteQuery("INSERT INTO object_values (id, value) VALUES (?, ?)", id, value)

	err := q.Exec()
	if err != nil {
		return err
	}

	return nil
}

func (self *CassandraStorage) RemObject(id int64) error {
	q := self.WriteQuery("DELETE FROM object_values WHERE id = ?", id)

	err := q.Exec()
	if err != nil {
		return err
	}

	return nil
}

func (self *CassandraStorage) AddObjectIndex(id int64, key string, value []byte) error {
	batch := self.session.NewBatch(gocql.LoggedBatch)
	batch.Cons = self.config.WriteConsistency

	q := self.ReadQuery("SELECT value FROM object_indexes WHERE id = ? AND key = ?",
		id,
		key)

	i := q.Iter()
	var oldValue []byte
	if i.Scan(&oldValue) {
		batch.Query("DELETE FROM object_indexes WHERE id = ? AND key = ?",
			id,
			key)

		batch.Query("DELETE FROM object_reverse_indexes WHERE key = ? AND value = ? AND id = ?",
			key,
			oldValue,
			id)
	}

	batch.Query("INSERT INTO object_indexes (id, key, value) VALUES (?, ?, ?)",
		id,
		key,
		value)

	batch.Query("INSERT INTO object_reverse_indexes (key, value, id) VALUES (?, ?, ?)",
		key,
		value,
		id)

	err := self.session.ExecuteBatch(batch)
	if err != nil {
		return err
	}

	return nil
}

func (self *CassandraStorage) RemObjectIndex(id int64, key string) error {
	batch := self.session.NewBatch(gocql.LoggedBatch)
	batch.Cons = self.config.WriteConsistency

	q := self.ReadQuery("SELECT value FROM object_indexes WHERE id = ? AND key = ?",
		id,
		key)

	i := q.Iter()
	var oldValue []byte
	if i.Scan(&oldValue) {
		batch.Query("DELETE FROM object_indexes WHERE id = ? AND key = ?",
			id,
			key)

		batch.Query("DELETE FROM object_reverse_indexes WHERE key = ? AND value = ? AND id = ?",
			key,
			oldValue,
			id)

		err := self.session.ExecuteBatch(batch)
		if err != nil {
			return err
		}
	}

	return nil
}

func (self *CassandraStorage) GetObjectsByIndex(key string, value []byte, limit int64) ([]ObjectIndex, error) {
	q := self.ReadQuery("SELECT id FROM object_reverse_indexes WHERE key = ? AND value = ? LIMIT ?",
		key,
		value,
		limit)

	objects := make([]ObjectIndex, 0, limit)

	i := q.Iter()
	for {
		var id int64
		if !i.Scan(&id) {
			err := i.Close()
			if err != nil {
				return nil, err
			}
			break
		}

		objects = append(objects, ObjectIndex{
			Id:    id,
			Key:   key,
			Value: value,
		})
	}

	return objects, nil
}

func (self *CassandraStorage) AddEdge(vertexFrom int64, label int64, weight int64, vertexTo int64, value []byte) error {
	q := self.ReadQuery("SELECT weight FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo = ?",
		vertexFrom,
		Outgoing,
		label,
		vertexTo)

	batch := self.session.NewBatch(gocql.LoggedBatch)
	batch.Cons = self.config.WriteConsistency

	countBatch := self.session.NewBatch(gocql.CounterBatch)
	countBatch.Cons = self.config.WriteConsistency

	hasOldWeight := false

	// delete the old weighted edges
	var oldWeight int64
	if q.Iter().Scan(&oldWeight) {
		batch.Query("DELETE FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight = ? AND vertexTo = ?",
			vertexFrom,
			Outgoing,
			label,
			oldWeight,
			vertexTo)

		batch.Query("DELETE FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight = ? AND vertexTo = ?",
			vertexTo,
			Incoming,
			label,
			oldWeight,
			vertexFrom)

		hasOldWeight = true
	}

	// update the unweighted edges
	batch.Query("INSERT INTO edge_weights (vertexFrom, state, label, vertexTo, weight, value) VALUES (?, ?, ?, ?, ?, ?)",
		vertexFrom,
		Outgoing,
		label,
		vertexTo,
		weight,
		value)

	batch.Query("INSERT INTO edge_weights (vertexFrom, state, label, vertexTo, weight, value) VALUES (?, ?, ?, ?, ?, ?)",
		vertexTo,
		Incoming,
		label,
		vertexFrom,
		weight,
		value)

	// insert new weighted edges
	batch.Query("INSERT INTO edge_values (vertexFrom, state, label, weight, vertexTo, value) VALUES (?, ?, ?, ?, ?, ?)",
		vertexFrom,
		Outgoing,
		label,
		weight,
		vertexTo,
		value)

	batch.Query("INSERT INTO edge_values (vertexFrom, state, label, weight, vertexTo, value) VALUES (?, ?, ?, ?, ?, ?)",
		vertexTo,
		Incoming,
		label,
		weight,
		vertexFrom,
		value)

	err := self.session.ExecuteBatch(batch)
	if err != nil {
		return err
	}

	// update the edge counts if no error has occurred, storing cardinality is enabled, and the edge already exists
	if self.config.StoreCardinality && !hasOldWeight {
		countBatch.Query("UPDATE edge_counts SET count = count + 1 WHERE vertexFrom = ? AND state = ? AND label = ?",
			vertexFrom,
			Outgoing,
			label)

		countBatch.Query("UPDATE edge_counts SET count = count + 1 WHERE vertexFrom = ? AND state = ? AND label = ?",
			vertexTo,
			Incoming,
			label)

		err = self.session.ExecuteBatch(countBatch)
		if err != nil {
			return err
		}
	}

	return nil
}

func (self *CassandraStorage) RemEdge(vertexFrom int64, label int64, vertexTo int64) error {
	q := self.ReadQuery("SELECT weight FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo = ?",
		vertexFrom,
		Outgoing,
		label,
		vertexTo)

	var oldWeight int64
	i := q.Iter()
	if !i.Scan(&oldWeight) {
		return i.Close()
	}

	batch := self.session.NewBatch(gocql.LoggedBatch)
	batch.Cons = self.config.WriteConsistency

	countBatch := self.session.NewBatch(gocql.CounterBatch)
	countBatch.Cons = self.config.WriteConsistency

	// delete the unweighted edges
	batch.Query("DELETE FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo = ?",
		vertexFrom,
		Outgoing,
		label,
		vertexTo)

	batch.Query("DELETE FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo = ?",
		vertexTo,
		Incoming,
		label,
		vertexFrom)

	// delete the weighted edges
	batch.Query("DELETE FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight = ? AND vertexTo = ?",
		vertexFrom,
		Outgoing,
		label,
		oldWeight,
		vertexTo)

	batch.Query("DELETE FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight = ? AND vertexTo = ?",
		vertexTo,
		Incoming,
		label,
		oldWeight,
		vertexFrom)

	err := self.session.ExecuteBatch(batch)
	if err != nil {
		return err
	}

	// update the edge counts if no error has occurred and storing cardinality is enabled
	if self.config.StoreCardinality {
		countBatch.Query("UPDATE edge_counts SET count = count - 1 WHERE vertexFrom = ? AND state = ? AND label = ?",
			vertexFrom,
			Outgoing,
			label)

		countBatch.Query("UPDATE edge_counts SET count = count - 1 WHERE vertexFrom = ? AND state = ? AND label = ?",
			vertexTo,
			Incoming,
			label)

		err = self.session.ExecuteBatch(countBatch)
		if err != nil {
			return err
		}
	}

	return nil
}

func (self *CassandraStorage) GetEdge(state EdgeState, vertexFrom int64, label int64, vertexTo int64) (*Edge, error) {
	q := self.ReadQuery("SELECT weight, value FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo = ?",
		vertexFrom,
		state,
		label,
		vertexTo)

	var weight int64
	var value []byte

	i := q.Iter()
	if !i.Scan(&weight, &value) {
		return nil, i.Close()
	}

	return &Edge{
		State:  state,
		From:   vertexFrom,
		Label:  label,
		Weight: weight,
		To:     vertexTo,
		Value:  value,
	}, nil
}

func (self *CassandraStorage) GetEdges(state EdgeState, vertexFrom int64, label int64, minWeight int64, maxWeight int64, limit int64) ([]Edge, error) {
	q := self.ReadQuery("SELECT vertexTo, weight, value FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight >= ? AND weight < ? LIMIT ?",
		vertexFrom,
		state,
		label,
		minWeight,
		maxWeight,
		limit)

	edges := make([]Edge, 0, limit)

	i := q.Iter()
	for {
		var vertexTo int64
		var weight int64
		var value []byte

		if !i.Scan(&vertexTo, &weight, &value) {
			err := i.Close()
			if err != nil {
				return nil, err
			}
			break
		}

		edges = append(edges, Edge{
			State:  state,
			From:   vertexFrom,
			Label:  label,
			Weight: weight,
			To:     vertexTo,
			Value:  value,
		})
	}

	return edges, nil
}

func (self *CassandraStorage) GetEdgesByVertex(state EdgeState, vertexFrom int64, label int64, minTo int64, maxTo int64, limit int64) ([]Edge, error) {
	q := self.ReadQuery("SELECT vertexTo, weight, value FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo >= ? AND vertexTo < ? LIMIT ?",
		vertexFrom,
		state,
		label,
		minTo,
		maxTo,
		limit)

	edges := make([]Edge, 0, limit)

	i := q.Iter()
	for {
		var vertexTo int64
		var weight int64
		var value []byte

		if !i.Scan(&vertexTo, &weight, &value) {
			err := i.Close()
			if err != nil {
				return nil, err
			}
			break
		}

		edges = append(edges, Edge{
			State:  state,
			From:   vertexFrom,
			Label:  label,
			Weight: weight,
			To:     vertexTo,
			Value:  value,
		})
	}

	return edges, nil
}

func (self *CassandraStorage) CountEdges(state EdgeState, vertexFrom int64, label int64, minWeight int64, maxWeight int64, limit int64) (int64, error) {
	q := self.ReadQuery("SELECT count(*) FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight >= ? AND weight < ? LIMIT ?",
		vertexFrom,
		state,
		label,
		minWeight,
		maxWeight,
		limit)

	var count int64
	i := q.Iter()
	if !i.Scan(&count) {
		return 0, i.Close()
	}

	return count, nil
}

func (self *CassandraStorage) RemoveEdges(state EdgeState, vertexFrom int64, label int64, minWeight int64, maxWeight int64, limit int64) (int64, error) {
	q := self.ReadQuery("SELECT vertexTo, weight FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight >= ? AND weight < ? LIMIT ?",
		vertexFrom,
		state,
		label,
		minWeight,
		maxWeight,
		limit)

	batch := self.session.NewBatch(gocql.LoggedBatch)
	batch.Cons = self.config.WriteConsistency

	countBatch := self.session.NewBatch(gocql.CounterBatch)
	countBatch.Cons = self.config.WriteConsistency

	count := int64(0)

	i := q.Iter()
	for {
		var vertexTo int64
		var weight int64

		if !i.Scan(&vertexTo, &weight) {
			err := i.Close()
			if err != nil {
				return 0, err
			}
			break
		}

		// delete the unweighted edge
		batch.Query("DELETE FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo = ?",
			vertexFrom,
			Outgoing,
			label,
			vertexTo)

		batch.Query("DELETE FROM edge_weights WHERE vertexFrom = ? AND state = ? AND label = ? AND vertexTo = ?",
			vertexTo,
			Incoming,
			label,
			vertexFrom)

		// delete the weighted edge
		batch.Query("DELETE FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight = ? AND vertexTo = ?",
			vertexFrom,
			Outgoing,
			label,
			weight,
			vertexTo)

		batch.Query("DELETE FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight = ? AND vertexTo = ?",
			vertexTo,
			Incoming,
			label,
			weight,
			vertexFrom)

		// update the edge counts
		if self.config.StoreCardinality {
			countBatch.Query("UPDATE edge_counts SET count = count - 1 WHERE vertexFrom = ? AND state = ? AND label = ?",
				vertexFrom,
				Outgoing,
				label)

			countBatch.Query("UPDATE edge_counts SET count = count - 1 WHERE vertexFrom = ? AND state = ? AND label = ?",
				vertexTo,
				Incoming,
				label)
		}

		count++
	}

	// return early if no edges have been deleted
	if count == 0 {
		return count, nil
	}

	err := self.session.ExecuteBatch(batch)
	if err != nil {
		return 0, err
	}

	// update the edge counts if we are storing cardinality
	if self.config.StoreCardinality {
		err = self.session.ExecuteBatch(countBatch)
		if err != nil {
			return count, err
		}
	}

	return count, nil
}

func (self *CassandraStorage) Cardinality(state EdgeState, vertexFrom int64, label int64) (int64, error) {
	q := self.ReadQuery("SELECT count FROM edge_counts WHERE vertexFrom = ? AND state = ? AND label = ?",
		vertexFrom,
		state,
		label)

	var count int64
	i := q.Iter()
	if !i.Scan(&count) {
		return 0, i.Close()
	}

	return count, nil
}

// Create new edges from a vertex with a label based on a range of edges,
// allowing a user to easily perform this action:
//
//   foreach (follower, weight, value) to a producer:
//     create a new edge: (newVertexFrom, newLabel, newWeight, follower, newValue)
//
func (self *CassandraStorage) Fanout(action EdgeAction, state EdgeState, vertexFrom int64, label int64, minWeight int64, maxWeight int64, limit int64, newVertexFrom int64, newLabel int64, newWeight int64, newValue []byte) (int64, error) {
	q := self.ReadQuery("SELECT vertexTo FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight >= ? AND weight < ? LIMIT ?",
		vertexFrom,
		state,
		label,
		minWeight,
		maxWeight,
		limit)

	count := int64(0)

	i := q.Iter()
	for {
		var vertexTo int64

		if !i.Scan(&vertexTo) {
			err := i.Close()
			if err != nil {
				return 0, err
			}
			break
		}

		// TODO: add a parameter to addedge/remedge that accepts a batch so we can group all of these updates together
		var err error
		if action == Add {
			err = self.AddEdge(newVertexFrom, newLabel, newWeight, vertexTo, newValue)
		} else if action == Rem {
			err = self.RemEdge(newVertexFrom, newLabel, vertexTo)
		}
		if err != nil {
			return count, err
		}
		count++
	}

	return count, nil
}

// Create new edges to a vertex with a label based on a range of edges,
// allowing a user to easily perform this action:
//
//   foreach (post, weight, value) from a producer:
//     create a new edge: (post, newLabel, weight, newVertexTo, value)
//
func (self *CassandraStorage) Fanin(action EdgeAction, state EdgeState, vertexFrom int64, label int64, minWeight int64, maxWeight int64, limit int64, newLabel int64, newVertexTo int64) (int64, error) {
	q := self.ReadQuery("SELECT vertexTo, weight, value FROM edge_values WHERE vertexFrom = ? AND state = ? AND label = ? AND weight >= ? AND weight < ? LIMIT ?",
		vertexFrom,
		state,
		label,
		minWeight,
		maxWeight,
		limit)

	count := int64(0)

	i := q.Iter()
	for {
		var vertexTo int64
		var weight int64
		var value []byte

		if !i.Scan(&vertexTo, &weight, &value) {
			err := i.Close()
			if err != nil {
				return 0, err
			}
			break
		}

		// TODO: add a parameter to addedge/remedge that accepts a batch so we can group all of these updates together
		var err error
		if action == Add {
			err = self.AddEdge(vertexTo, newLabel, weight, newVertexTo, value)
		} else if action == Rem {
			err = self.RemEdge(vertexTo, newLabel, newVertexTo)
		}
		if err != nil {
			return count, err
		}
		count++
	}

	return count, nil
}

/////////////
// Queries //
/////////////

func (self *CassandraStorage) ReadQuery(stmt string, args ...interface{}) *gocql.Query {
	q := self.session.Query(stmt, args...)
	q = q.Consistency(self.config.ReadConsistency)
	q = q.PageSize(self.config.PageSize)
	q = q.Prefetch(self.config.PagePrefetchRatio)
	return q
}

func (self *CassandraStorage) WriteQuery(stmt string, args ...interface{}) *gocql.Query {
	q := self.session.Query(stmt, args...)
	q = q.Consistency(self.config.WriteConsistency)
	return q
}
