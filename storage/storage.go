package storage

var (
	Outgoing EdgeState = 0
	Incoming EdgeState = 1
	Archived EdgeState = 2

	Add EdgeAction = 0
	Rem EdgeAction = 1
)

type EdgeState uint8
type EdgeAction uint8

type Object struct {
	Id    int64
	Type  string
	Value []byte
}

type ObjectIndex struct {
	Id    int64
	Key   string
	Value []byte
}

type Edge struct {
	State  EdgeState
	From   int64
	Label  int64
	Weight int64
	To     int64
	Value  []byte
}

type Storage interface {
	// Info
	Type() string
	Path() string

	// Ids
	GetUniqueId() int64

	// Objects
	NewObject(otype string, value []byte) (*Object, error)
	GetObject(id int64) (*Object, error)
	SetObject(id int64, value []byte) error
	RemObject(id int64) error

	// Index
	AddObjectIndex(id int64, key string, value []byte) error
	RemObjectIndex(id int64, key string) error
	GetObjectsByIndex(key string, value []byte, limit int64) ([]ObjectIndex, error)

	// Edge
	AddEdge(fromVertex int64, label int64, weight int64, toVertex int64, value []byte) error
	RemEdge(fromVertex int64, label int64, toVertex int64) error

	// Edges
	GetEdge(state EdgeState, fromVertex int64, label int64, toVertex int64) (*Edge, error)
	GetEdges(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64) ([]Edge, error)
	GetEdgesByVertex(state EdgeState, fromVertex int64, label int64, minTo int64, maxTo int64, limit int64) ([]Edge, error)
	CountEdges(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64) (int64, error)
	RemoveEdges(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64) (int64, error)
	Cardinality(state EdgeState, fromVertex int64, label int64) (int64, error)
	Fanout(action EdgeAction, state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64, newFromVertex int64, newLabel int64, newWeight int64, newValue []byte) (int64, error)
	Fanin(action EdgeAction, state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64, newLabel int64, newToVertex int64) (int64, error)
}
