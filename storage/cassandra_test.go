package storage

// TODO: implement tests for cassandra storage

/*
Tests:
    GetUniqueId() int64
    - test that ids returned are guaranteed unique

	// Objects
	NewObject(otype string, value []byte) (*Object, error)
	GetObject(id int64) (*Object, error)
	SetObject(id int64, value []byte) error
	RemObject(id int64) error
    - test that adding and removing an object works
    - test that setting an object's then getting that value returns the written value (read what you write)

	// Index
	AddObjectIndex(id int64, key string, value []byte) error
	RemObjectIndex(id int64, key string) error
	GetObjectsByIndex(key string, minValue []byte, maxValue []byte, limit int64) ([]ObjectIndex, error)
	- test indexing an object by key/value pairs allows one to retreive the object

	// Edge
	AddEdge(fromVertex int64, label int64, weight int64, toVertex int64, value []byte) error
	RemEdge(fromVertex int64, label int64, toVertex int64) error
	- test that adding and removing edges
	- test that adding and removing an edge updates the cardinality count

	// Edges
	GetEdge(state EdgeState, fromVertex int64, label int64, toVertex int64) (*Edge, error)
	GetEdges(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64) ([]Edge, error)
	GetEdgesByVertex(state EdgeState, fromVertex int64, label int64, minTo int64, maxTo int64, limit int64) ([]Edge, error)
	CountEdges(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64) (int64, error)
	RemoveEdges(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64) (int64, error)
	Cardinality(state EdgeState, fromVertex int64, label int64) (int64, error)
	Fanout(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64, newFromVertex int64, newLabel int64, newWeight int64, newValue []byte) (int64, error)
	Fanin(state EdgeState, fromVertex int64, label int64, minWeight int64, maxWeight int64, limit int64, newLabel int64, newToVertex int64) (int64, error)
	- test that edges can be retreived in weighted order (reverse)
	- test that edges can be retreived in vertex order (reverse)
	- test that bulk removal of edges updates the cardinality count
	- test that the cardinality and countedges values are the same for a query
	- test that fanout creates the appropriate edges
	- test that fanin creates the appropriate edges
*/
