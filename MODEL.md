# Graph Models

You can model a lot of different things with graphs. Actually, most complex objects can be decomposed into edges and vertices.

Most relational models can be modeled in a graph database. 

| RDBS        | GRAPH       |
|-------------|-------------|
| Table       | Vertex Type |
| Row         | Vertex      |
| Foreign Key | Edge        |

Say, for example, we have two RDBM table "invoice" and "person".

| Invoice   |                                  ||
|-----------|-------------------------|---------|
| id        | primary key             | integer |
| sent_from | foreign key (person.id) | integer |
| sent_to   | foreign key (person.id) | integer |
| amount    |                         | integer |

| Person |                                  ||
|--------|-------------------------|---------|
| id     | primary key             | integer |
| name   |                         | varchar |

Both an Invoice and a Person would be represented as a vertex in the graph. 
Then, an edge between each would be added to define the sent edge label (i.e., relationship):

    user1 => name:Fred
    user2 => name:Frank
    invoice1 => amount:500
    invoice1 -> sent_from -> user1
    invoice1 -> sent_to -> user2

But, the edges also can have values, so you can duplicate data to make queries easier:

    invoice1 -> sent -> user2 => from:user1

The nice thing, though, is that edges can be formed between any vertexes.

    company1 -> created -> invoice1
    company1 -> sent -> user1 (weight:=invoice1)


## Examples

Here are some examples.

### Twitter ([via](https://blog.twitter.com/2010/introducing-flockdb))

- Vertices
    - user
    - tweet
- Edges
    - follows
    - tweets
    - timeline:home

Example:

    user1 -> follows -> user2
    user2 -> tweets -> tweet = (id, text, mentions, etc)

    fanout:
        foreach follower of user2:
            tweet -> timeline:home -> :follower

### Pinterest ([via](http://engineering.pinterest.com/post/55272557617/building-a-follower-model-from-scratch))

- Vertices
    - user
    - board
- Edges
    - follows:explicit
    - follows:implicit
    - unfollows:explicit

Example:

    user1 -> follows:explicit -> user2

    user1 -> follows:explicit -> board1
    user1 -> follows:implicit -> user3

    user1 -> unfollows:explicit -> board17


### Facebook ([via](https://www.facebook.com/notes/facebook-engineering/tao-the-power-of-the-graph/10151525983993920))

- Vertices
    - user
    - location
    - comment
    - checkin
- Edges
    - friend
    - authored
    - tagged
    - liked
    - commented

Example: 
    
    user105 -> friend -> user244
    user244 -> friend -> user379
    user379 -> friend -> user471

    user105 -> authored -> checkin632
    checkin632 -> checkedin -> location534

    user244 -> tagged_at -> checkin632

    user379 -> authored -> comment771
    comment771 -> commented -> checkin632

    user471 -> liked -> comment771

### Soundcloud ([via](https://developers.soundcloud.com/blog/roshi-a-crdt-system-for-timestamped-events))

- Vertices
    - user
    - track
    - event
- Edges
    - posted
    - reposted

Example:

    scrillex -> posted -> track:duck-sauce
    user:a-trak -> reposted -> track:duck-sauce

### Blogs

- Vertices
    - user
    - post
- Edges
    - published
    - commented

Example:

    user1 -> published -> post1 (id, name, title)
    
    user2 -> commented -> comment1
    comment1 -> commented -> post1 = (comment)

    user3 -> commented -> comment2
    comment2 -> commented -> comment1 = (comment)

### Generic Messaging / Feed Reader

- Vertices
    - consumer (user|folder)
    - producer (user|feed)
    - entry
- Edges
    - follows
    - inbox  (aka: sent)
    - outbox (aka: authored)
    - creates

Example:

    user1 -> follows -> producer1
    user1 -> follows -> producer2
    user1 -> follows -> producer3

    user1 -> creates -> folder1
    folder1 -> follows -> producer2
    folder1 -> follows -> producer3

    feed1 -> authored -> entry1

    fanout:
        foreach follower of producer1:
            entry1 -> sent -> :follower

    folder1 -> unfollows -> producer2

    unfanin: 
        foreach entry of producer2:
            remove :entry -> sent -> folder1

### Hyperedges ([via](http://neo4j.com/docs/2.1.5/cypher-cookbook-hyperedges.html))

- Vertices
    - user
    - role
    - group
- Edges
    - hasRoleInGroup
    - hasRole
    - hasGroup
    - in
    - isA
    - canHave

Example:

    user1 -> in -> group1
    user1 -> in -> group2
    user1 -> hasRoleInGroup -> user1:role1:group2
    user1 -> hasRoleInGroup -> user1:role2:group1
    user1:role1:group2 -> hasRole -> role1
    user1:role1:group2 -> hasGroup -> group2
    user1:role2:group1 -> hasRole -> role2
    user1:role2:group1 -> hasGroup -> group1
    group1 -> canHave -> role1
    group1 -> canHave -> role2
    group2 -> canHave -> role1
    group2 -> canHave -> role2
    user1 -> isA -> user
    group1 -> isA -> group
    group2 -> isA -> group
    role1 -> isA -> role
    role2 -> isA -> role

### Using a weight as a context pointer instead of an ordering timestamp

- Vertices
    - user
    - location
- Edge
    - checkin
    - at
    - taggedAt

Example:

    user1 -> checkin -> checkin1
    checkin1 -> at -> location1
    user2 -> taggedAt -> checkin1 (weight[by]: user1)

